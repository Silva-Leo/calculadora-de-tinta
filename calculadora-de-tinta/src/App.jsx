import React from 'react'
import GlobalStyle from './style/GlobalStyle'
import Layout from './components/Layout/Layout';

function App() {
  return (
    <>
    <GlobalStyle/>
    <Layout/>
    </>
  )
}

export default App
