import React from "react";
import { Container } from "../Container/Container";
import Header from "../Header/Header";
import { Section, SectionTitulo, SectionInput } from "../Container/Section";
import { InputComum } from "../Inputs/InputComum/Input";
import { InputResultado } from "../Inputs/InputResultado/inputResultado";
import { Titulo } from "../Titulo/Titulo";
import { BotaoAdd } from "../Buttons/BotaoAdd/BotaoAdd.style";

function Layout() {
    return (
        <>
            <Header />
            <Container>
                <Section>
                    <SectionTitulo>
                        <Titulo titulo="Dimensão parede:" />
                    </SectionTitulo>
                    <SectionInput>
                        <InputComum label="Altura (m) " />
                        <InputComum label="Largura (m) " />
                        <InputResultado label="Resultado (m²):" />
                    </SectionInput>
                        <BotaoAdd>Adicionar</BotaoAdd>
                </Section>
                <Section>
                    <SectionTitulo>
                        <Titulo titulo="Dimensão porta:" />
                    </SectionTitulo>
                    <SectionInput>
                        <InputComum label="Altura (m) " />
                        <InputComum label="Largura (m) " />
                        <InputResultado label="Resultado (m²):" />
                    </SectionInput>
                        <BotaoAdd>Adicionar</BotaoAdd>
                </Section>
                <Section>
                    <SectionTitulo>
                        <Titulo titulo="Dimensão janela: " />
                    </SectionTitulo>
                    <SectionInput>
                        <InputComum label="Altura (m) " />
                        <InputComum label="Largura (m) " />
                        <InputResultado label="Resultado (m²):" />
                    </SectionInput>
                        <BotaoAdd>Adicionar</BotaoAdd>
                </Section>
            </Container>
        </>
    );
}

export default Layout;
