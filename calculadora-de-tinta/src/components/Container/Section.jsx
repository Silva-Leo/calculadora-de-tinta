import styled from 'styled-components';
import { Container } from "./Container";

export const Section = styled(Container)`
    width: 100%;
    height:100%;
    justify-content: space-evenly;
    align-items: flex-start;
    padding-left: 5%;
`

export const SectionTitulo = styled(Container)`
    height:25%;
    width: 100%;
    display: flex;
    justify-content: flex-start;
`

export const SectionInput = styled(Container)`
    flex-direction: row;
    height:fit-content;
    justify-content: space-between;
    width:80%;
`