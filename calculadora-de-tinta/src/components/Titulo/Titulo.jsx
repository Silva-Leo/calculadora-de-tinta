import React from 'react';

export function Titulo(props){
  return (
    <h2>{props.titulo}</h2>
  );
}
