import styled from 'styled-components';

export const HeaderStyle=styled.div`
    height: 12vh;
    width: 100%;
    display: flex;
    background-color: transparent;
    justify-content: flex-start;
    padding: 1rem;
`