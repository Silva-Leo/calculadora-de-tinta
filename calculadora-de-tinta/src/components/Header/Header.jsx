import React from 'react';
import { HeaderStyle } from './Header.style';
import logo from "../../assets/logologo.png"

export default function Header() {
  return (
    <>
        <HeaderStyle>
            <img src={logo} alt="logo"/>
        </HeaderStyle>
    </>
  );
}
