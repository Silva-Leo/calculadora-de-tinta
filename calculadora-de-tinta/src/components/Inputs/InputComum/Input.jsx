import React from "react";
import { Input } from "./Input.style";
import { Label } from "../Label/label";

export function InputComum(props) {
    return (
        <Label>
            {props.label}
            <Input type="number" placeholder="Digite o valor"></Input>
        </Label>
    );
}
