import styled from "styled-components";

export const Input = styled.input`
    height: 3rem;
    width: 9rem;
    font-size: 1rem;
    background: #e9e9e9e6;
    border: thin solid black;
    border-radius: 0.4rem;

    &:hover{
        border: 2px solid black;
    }
    
    ::placeholder {
        color: #000000;
        text-align: center;
        letter-spacing: 0.1rem;
    }


    :focus::placeholder{
        opacity: 0;
    }

    
`;
