import styled from "styled-components";
import { Input } from "../InputComum/Input.style";

export const InputResulStyle = styled(Input)`
    width: 8rem;
    border: 0;
    border-radius: 0;
    border-bottom: 1px solid;

    &:hover{
        border:none;
    }
`;
