import React from "react";
import { InputResulStyle } from "./inputResultado.style";
import { Label } from "../Label/label";

export function InputResultado(props) {
    return (
        <Label>
            {props.label}
            <InputResulStyle type="number"></InputResulStyle>
        </Label>
    );
}
