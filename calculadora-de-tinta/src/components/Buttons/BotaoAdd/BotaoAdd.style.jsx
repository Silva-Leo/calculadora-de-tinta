import styled from "styled-components";

export const BotaoAdd = styled.button`
    height: 3rem;
    width: 8rem;
    font-size: smaller;
    background-color: #058AC5;
    border: thin solid #0071a1;
    border-radius: 0.4rem;
    color: #ffffff;

    &:hover{
        background-color: #00364b;
    }
    
`;